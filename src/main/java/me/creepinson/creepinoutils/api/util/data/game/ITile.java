package me.creepinson.creepinoutils.api.util.data.game;

import me.creepinson.creepinoutils.api.util.SerializableString;

/**
 * @author Creepinson http://gitlab.com/creepinson
 **/
public interface ITile extends SerializableString {

}
