package me.creepinson.creepinoutils.api.util;

public interface Module {

    void onEnable();

    void onReload();

    void onDisable();

}
