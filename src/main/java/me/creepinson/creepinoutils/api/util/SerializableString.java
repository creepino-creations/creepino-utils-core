package me.creepinson.creepinoutils.api.util;

import java.io.Serializable;

/**
 * @author Creepinson http://gitlab.com/creepinson
 **/
public interface SerializableString extends Serializable {
    String getName();
}